Video
=====

Video is a CMSMS plugin that allows you to easily display an ADA compliant, XHTML video in Quicktime format.

How do I use it?
----------------

Place the following tag in your template or content:

```html
{video url="" width="" height=""}
```

What parameters can I use with Video?
-------------------------------------

+ (required) url - Relative URL of video
+ (required) width - Width of video
+ (required) height - Height of video
+ (optional) autoplay - Autoplay Video (set 1 or true for use)
+ (optional) controls - Show controls (set 1 or true for use)