<?php
	
	/*

	function.video.php
	-------------------

	@file 		function.video.php
	@version	0.1
	@date 		2010-07-20 09:25:14 -0500 (Tue, 20 Jul 2010)
	@author 	Kenton Glass [kenton@notnek.com]

	Copyright (c) 2010 Kenton Glass [http://knt.gs]

	*/
	
	
	# Video Function
	function smarty_cms_function_video($params, &$smarty){

		# If Controller, Add Height
		if($params['controls']):
	
			# True
			$params['controls'] = 'true';
	
			# Add Height
			$params['height'] = $params['height'] + 16;

		# No Controller
		else:

			# False
			$params['controls'] = 'false';

		# End No Controller
		endif;

		# Autoplay
		if($params['autoplay']):
			
			# True
			$params['autoplay'] = 'true';
		
		# No Autoplay
		else:
		
			# False
			$params['autoplay'] = 'false';
		
		# End No Autoplay
		endif;

		print '<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab" width="'.$params['width'].'" height="'.$params['height'].'">'."\n"
			 .'	<param name="src" value="'.$params['url'].'" />'."\n"
			 .'	<param name="autoplay" value="'.$params['autoplay'].'" />'."\n"
			 .'	<param name="pluginspage" value="http://www.apple.com/quicktime/download/" />'."\n"
			 .'	<param name="controller" value="'.$params['controls'].'" />'."\n"
			 .'	<!--[if !IE]> <-->'."\n"
			 .'		<object data="'.$params['url'].'" width="'.$params['width'].'" height="'.$params['height'].'" type="video/quicktime">'."\n"
		     .'			<param name="pluginurl" value="http://www.apple.com/quicktime/download/" />'."\n"
		     .'			<param name="controller" value="'.$params['controls'].'" />'."\n"
		   	 .'		</object>'."\n"
		   	 .'	<!--> <![endif]-->'."\n"
			 .'</object>'."\n"
			 .'<br />Don&#39;t have the plugin? <a href="http://www.apple.com/quicktime/download/" rel="external">Click here to download it.</a>'."\n";
	
	# End Video Function
	}
	
	# Help Function
	function smarty_cms_help_function_video(){

?>
<h3>What is Video?</h3>
	<p>Video is a plugin that allows you to easily display an ADA compliant, XHTML video in Quicktime format.</p>
<h3>How do I use it?</h3>
	<p>Place the following tag in your template or content: <code>{video url="" width="" height=""}</code></p>
<h3>What parameters can I use with Video?</h3>
	<p>
		<ul>
			<li><em>(required)</em> <tt>url</tt> - Relative URL of video.</li>
			<li><em>(required)</em> <tt>width</tt> - Width of video.</li>
			<li><em>(required)</em> <tt>height</tt> - Height of video.</li>
			<li><em>(optional)</em> <tt>autoplay</tt> - Autoplay Video (set 1 or true for use).</li>
			<li><em>(optional)</em> <tt>controls</tt> - Show controls (set 1 or true for use).</li>
		</ul>
	</p>
<?php 

	# End Help Function
	}
	
	# About Function
	function smarty_cms_about_function_video(){

?>
<p>Author: <a href="http://www.notnek.com/" rel="external">Kenton Glass</a></p>
<p>Version: 0.1</p>
<ul>
	<li>Initial Release</li>
</ul>
<?php

	# End About Function
	}
	
?>